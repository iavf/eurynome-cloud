/*
 * Copyright (c) 2019-2021 Gengwei Zheng (herodotus@aliyun.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project Name: eurynome-cloud
 * Module Name: eurynome-cloud-assistant
 * File Name: SymbolConstants.java
 * Author: gengwei.zheng
 * Date: 2021/10/17 22:56:17
 */

package cn.herodotus.eurynome.assistant.constant;

/**
 * @author gengwei.zheng
 */
public class SymbolConstants {

	public static final String AMPERSAND = "&";

	public static final String AMPERSAND_ENCODED = "&amp;";

	public static final String APOSTROPHE = "'";

	public static final String APOSTROPHE_AND_COMMA = "',";

	public static final String APOSTROPHE_AND_COMMA_AND_APOSTROPHE = "','";

	public static final String AT = "@";

	public static final String BACK_SLASH = "\\";

	public static final String BETWEEN = "BETWEEN";

	public static final String BLANK = "";

	public static final String CDATA_OPEN = "<![CDATA[";

	public static final String CDATA_CLOSE = "]]>";

	public static final String CLOSE_BRACKET = "]";

	public static final String CLOSE_CURLY_BRACE = "}";

	public static final String CLOSE_PARENTHESIS = ")";

	public static final String COLON = ":";

	public static final String COMMA = ",";

	public static final String COMMA_AND_APOSTROPHE = ",'";

	public static final String COMMA_AND_SPACE = ", ";

	public static final String DASH = "-";

	public static final String DOUBLE_APOSTROPHE = "''";

	public static final String DOUBLE_CLOSE_BRACKET = "]]";

	public static final String DOUBLE_OPEN_BRACKET = "[[";

	public static final String DOUBLE_SLASH = "//";

	public static final String EQUAL = "=";

	public static final String GREATER_THAN = ">";

	public static final String GREATER_THAN_OR_EQUAL = ">=";

	public static final String FORWARD_SLASH = "/";

	public static final String FOUR_SPACES = "    ";

	public static final String FINISH_LEFT_ANGLE = "</";

	public static final String FINISH_RIGHT_ANGLE = "/>";

	public static final String GBK = "GBK";

	public static final String IS_NOT_NULL = "IS NOT NULL";

	public static final String IS_NULL = "IS NULL";

	public static final String IN = "IN";

	public static final String LEFT_ANGLE = "<";

	public static final String LESS_THAN = "<";

	public static final String LESS_THAN_OR_EQUAL = "<=";

	public static final String LIKE = "LIKE";

	public static final String MINUS = "-";

	public static final String NBSP = "&nbsp;";

	public static final String NEW_LINE = "\n";

	public static final String NOT_EQUAL = "!=";

	public static final String DB_NOT_EQUAL = "<>";

	public static final String NOT_LIKE = "NOT LIKE";

	public static final String NULL = "null";

	public static final String OPEN_BRACKET = "[";

	public static final String OPEN_CURLY_BRACE = "{";

	public static final String OPEN_PARENTHESIS = "(";

	public static final String PERCENT = "%";

	public static final String PERIOD = ".";

	public static final String PIPE = "|";

	public static final String PLUS = "+";

	public static final String POUND = "#";

	public static final String QUESTION = "?";

	public static final String QUOTE = "\"";

	public static final String RETURN = "\r";

	public static final String RETURN_NEW_LINE = "\r\n";

	public static final String RIGHT_ANGLE = ">";

	public static final String SEMICOLON = ";";

	public static final String SLASH = FORWARD_SLASH;

	public static final String SPACE = " ";

	public static final String STAR = "*";

	public static final String TAB = "\t";

	public static final String TILDE = "~";

	public static final String UNDERLINE = "_";

	public static final String SUFFIX_EXCEL_2003 = ".xls";

	public static final String SUFFIX_EXCEL_2007 = ".xlsx";

	public static final String SUFFIX_JPEG = ".jpg";

	public static final String SUFFIX_XML = ".xml";

	public static final String SUFFIX_PDF = ".pdf";

	public static final String SUFFIX_ZIP = ".zip";

	public static final String SUFFIX_DOC = ".doc";

	public static final String SUFFIX_DOCX = ".docx";

	public static final String SUFFIX_PPT = ".ppt";

	public static final String SUFFIX_PPTX = ".pptx";

	public static final String SUFFIX_EXCEL = ".xls";

	public static final String SUFFIX_EXCELX = ".xlsx";

	public static final String SUFFIX_SWF = ".swf";

	public static final String SUFFIX_PROPERTIES = ".properties";

	public static final String SUFFIX_YML = ".yml";

	public static final String SUFFIX_YAML = ".yaml";

	public static final String SUFFIX_JSON = ".json";

	public static final String XML_DECLARATION = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
}
